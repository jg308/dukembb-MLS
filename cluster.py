from pyemd import emd
import numpy as np
from sklearn.cluster import spectral_clustering

# fgpDist = {}
# playerId = {} #map player's id to 0,1,...

# #create identity matrix
# identity_matrix = np.identity(102)
# #print(identity_matrix)

# #create a distance matrix for all players
# distance_matrix = np.zeros((294,294))

# count = 0
# DATA = open('fgp-distribution.csv','r')
# #output = open('valid-players.csv','w')
# for line in DATA:
# 	pid,raw = line.split(':')
# 	distribution = raw.split(',')
# 	fgpDist[pid] = np.array(list(map(float,distribution)))
# 	playerId[pid] = count
# 	count = count + 1
# 	#print("{}".format(pid),file=output)
# DATA.close()
# #output.close()

# for item1 in fgpDist.keys():
# 	for item2 in fgpDist.keys():
# 		if(item1 == item2):
# 			distance_matrix[playerId[item1],playerId[item2]] = 0
# 		else:
# 			distance_matrix[playerId[item1],playerId[item2]] = emd(fgpDist[item1],fgpDist[item2],identity_matrix)

# labels = spectral_clustering(distance_matrix, n_clusters=5, eigen_solver='arpack')
# labelfile = open('cluster-5-kmeans.csv','w')
# for l in zip(fgpDist.keys(),labels):
# 	print("{}|{}".format(l[0],l[1]),file=labelfile)
# labelfile.close()


ftpDist = {}
playerId = {} #map player's id to 0,1,...

#create identity matrix
identity_matrix = np.identity(102)
#print(identity_matrix)

#create a distance matrix for all players
distance_matrix = np.zeros((400,400))

count = 0
DATA = open('ftp-distribution.csv','r')
#output = open('valid-players.csv','w')
for line in DATA:
	pid,raw = line.split(':')
	distribution = raw.split(',')
	ftpDist[pid] = np.array(list(map(float,distribution)))
	playerId[pid] = count
	count = count + 1
	#print("{}".format(pid),file=output)
DATA.close()
#output.close()

for item1 in ftpDist.keys():
	for item2 in ftpDist.keys():
		if(item1 == item2):
			distance_matrix[playerId[item1],playerId[item2]] = 0
		else:
			distance_matrix[playerId[item1],playerId[item2]] = emd(ftpDist[item1],ftpDist[item2],identity_matrix)

labels = spectral_clustering(distance_matrix, n_clusters=5, eigen_solver='arpack')
labelfile = open('cluster-5-kmeans-ftp.csv','w')
for l in zip(ftpDist.keys(),labels):
	print("{}|{}".format(l[0],l[1]),file=labelfile)
labelfile.close()