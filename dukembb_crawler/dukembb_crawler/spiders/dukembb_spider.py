import scrapy

class DukembbSpider(scrapy.Spider):
	name='dukembb'


	def start_requests(self):
		url = ['http://goduke.statsgeek.com/basketball-m/players/all.php']
		for item in url:
			yield scrapy.Request(url=item, callback=self.parse)
           


	def parse(self,response):

		players = []
		position = []
		
		for item in response.css("a[class=stattextline]::text").extract():
			players.append(str(item).strip())
		attrs = response.css("td[class=stattextline][align=center]::text").extract()
		for item in attrs[0:len(attrs)-1:4]:
			position.append(item.encode('ascii','ignore').decode('utf-8').strip())
		assert(len(players) == len(position))
		with open("player-position.csv","w") as OUTPUT:
			for item in zip(players,position):
				print("{}|{}".format(item[0],item[1]),file=OUTPUT)
		OUTPUT.close()
		# print(len(players))
		# print(len(position))

