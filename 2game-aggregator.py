from collections import defaultdict

def trueShootPer(tp,fga,fta):
	return tp/float(2*fga + float(0.88*fta))

def expectation(l):

	fga=defaultdict(lambda:0)
	fta=defaultdict(lambda:0)
	fga_expected = 0
	fta_expected = 0
	for t in l:
		fga[t[2]] += t[4]
		fta[t[3]] += t[4]

	for key in fga:
		fga_expected += key * fga[key]
	for key in fta:
		fta_expected += key * fta[key]
	return int(fga_expected)+1,int(fta_expected)+1

player_performance = defaultdict(list)
player_performance_2aggregated = defaultdict(list)
pid_gid = []

with open('id-new.dat','r') as f1:
	for line in f1:
		pid,gid,null = line.split(',')
		pid_gid.append((pid,gid))
	f1.close()

with open('points-with-fgafta-new.dat','r') as f2:
	count = 0
	for line in f2:
		pid,gid = pid_gid[count]
		records = line.split('|')[:-1]
		single_game = []
		for r in records:

			tp,fga,fta,prob,null = r.split(',')
			tp = int(tp)
			fga = int(fga)
			fta = int(fta)
			prob = float(prob)
			single_game.append((gid,tp,fga,fta,prob))
		player_performance[pid].append(single_game)
		count += 1
	f2.close()

for key in player_performance.keys():
	sorted(player_performance[key],key=lambda s:s[0][0])

# print(expectation(player_performance["301"][2]))
ids = open('id.dat','w')
output = open('point.dat','w')
for key in player_performance.keys():
	assert(len(player_performance[key]) != 0)
	if len(player_performance[key]) == 1:
		continue
		# if(len(player_performance[key][0]) == 1):
		# 	gid = player_performance[key][0][0]
		# 	result = [(x[1],trueShootPer(x[1],x[2],x[3]),x[4]) for x in player_performance[key][0]]
		# 	print("{},{},".format(key,gid),file=ids)
		# 	print("|".join("%d,%f,%f," % x for x in result) + '|', file=output)
		# else:
		# 	gid = player_performance[key][0][0]
		# 	print("{},{},".format(key,gid),file=ids)
		# 	ans = [(x[1],trueShootPer(x[1],x[2],x[3]),x[4]) for x in player_performance[key][0]]
		# 	print("|".join("%d,%f,%f," % x for x in ans) + '|', file=output)
	else:
		#more than one game, do 2-game aggregate stats for tsp
		for i in range(0,len(player_performance[key])-1):
			curr = player_performance[key][i]
			next = player_performance[key][i+1]
			if len(curr) == 1 and len(next) == 1:
				print("{},{},".format(key,curr[0][0]),file=ids)
				print("{},{},{},|".format(curr[0][1]+next[0][1],
					trueShootPer(curr[0][1]+next[0][1],curr[0][2]+next[0][2],curr[0][3]+next[0][3]),1),file=output)

			elif len(curr) > 1 and len(next) == 1:
				print("{},{},".format(key,curr[0][0]),file=ids)
				ans = [(x[1]+next[0][1],trueShootPer(x[1]+next[0][1],x[2]+next[0][2],x[3]+next[0][3]),x[4]) for x in curr]
				print("|".join("%d,%f,%f," % x for x in ans) + '|', file=output)

			elif len(curr) == 1 and len(next) > 1:
				print("{},{},".format(key,curr[0][0]),file=ids)
				ans = [(x[1]+curr[0][1],trueShootPer(x[1]+curr[0][1],x[2]+curr[0][2],x[3]+curr[0][3]),x[4]) for x in next]
				print("|".join("%d,%f,%f," % x for x in ans) + '|', file=output)

			elif len(curr) > 1 and len(next) > 1:
				print("{},{},".format(key,curr[0][0]),file=ids)
				e_fga,e_fta = expectation(next)
				ans = [(x[1]+next[0][1],trueShootPer(x[1]+next[0][1],x[2]+e_fga,x[3]+e_fta),x[4]) for x in curr]
				print("|".join("%d,%f,%f," % x for x in ans) + '|', file=output)
			else:
				print("ERROR! Shouldn't be here!")
ids.close()
output.close()


