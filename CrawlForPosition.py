import urllib.request
import psycopg2
from bs4 import BeautifulSoup
import re


def findPosition(errors):
	output = open('pid-position.csv','w')
	for item in errors:
		
		url = "http://goduke.statsgeek.com/basketball-m/players/statlines.php?playerid={}".format(item)
		response = urllib.request.urlopen(url).read()
		
		soup = BeautifulSoup(response, 'html.parser')
		
		position_text = soup('td',valign='top',align='left')[1].find('p').find_all('span')[2].text.split(' ')[-1]
		position = ""
		match = re.compile(r'[A-Z]/*[A-Z]*').match(position_text)
		if match is not None:
			if match.group() in ["G","G/F","F/G","F","F/C","C/F","C"]:
				position = match.group()
				print("{}:{} - {}".format(item,position_text,position))
				print("{}|{}".format(item,position),file=output)
			else:
				continue
		else:
			print(item)
			continue
	output.close()
		#break

if __name__ == '__main__':

	conn = psycopg2.connect("dbname=dukembb user=vagrant")
	if conn:
		print("Connected!")
	else:
		print("Connection Error")

	cur = conn.cursor()
	cur.execute("select distinct pid from pgstats;")
	pids = cur.fetchall()
	errorList = []
	for pid in pids:
		errorList.append(pid[0])
	print("{} players in total".format(len(errorList)))
	findPosition(errorList)


