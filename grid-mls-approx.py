import bisect
import collections as clt
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

class GridMLSApprox:
	def __init__(self,point_file,data_file):
		self.pf = point_file
		self.df = data_file
		self.points = []
		self.x_sorted = []
		self.y_sorted = []

	def parse_file(self):
		#point format: (x,y,probability of existence,id)
		pts = open(self.pf,'r')
		ids = open(self.df,'r')
		#fill points,x_sorted,y_sorted

		for line in zip(pts,ids):
			pts_line = line[0].strip()
			id_line = line[1].strip()
			#print(pts_line)
			#print(id_line)
			for item in pts_line.split('|')[:-1]:
				self.points.append((int(item.split(',')[0].strip()),
					float(item.split(',')[1].strip()),
					float(item.split(',')[2].strip()),
					id_line))

		#print("total points:{}\n".format(len(self.points)))

		for idx in range(0,len(self.points)):
			self.x_sorted.append((self.points[idx][0],idx))
			self.y_sorted.append((self.points[idx][1],idx))

		self.x_sorted.sort(key=lambda x:x[0])
		self.y_sorted.sort(key=lambda y:y[0])

		pts.close()
		ids.close()

		return self.x_sorted[-1][0],self.y_sorted[-1][0]

	def range_search(self,range):
		#range is defined as x>=range.x && y >= range.y
		x = range[0]
		y = range[1]
		#print(x,y)
		x_index = bisect.bisect_right(self.x_sorted,(x,)) 
		y_index = bisect.bisect_right(self.y_sorted,(y,))

		#return the index of the element that strictly greater than (> not >=) the query point
		if self.x_sorted[x_index][0] == x:
			x_index += 1
		if self.y_sorted[y_index][0] == y:
			y_index += 1

		#print(x_index,y_index)

		x_res = [self.points[item[1]] for item in self.x_sorted[x_index:]]
		y_res = [self.points[item[1]] for item in self.y_sorted[y_index:]]

		return set(x_res) & set(y_res)

	def range_search_check(self,range):
		x = range[0]
		y = range[1]

		res_range_search = self.range_search(range)
		res_bf = []

		for item in self.points:
			if item[0] >= x and item[1] >= y:
				res_bf.append(item)

		print(res_range_search == set(res_bf))

	def skyline_prob(self,cell):

		pointset = self.range_search(cell)
		#print(pointset)
		pts_dict = clt.defaultdict(list)

		for p in pointset:
			pts_dict[p[-1]].append(p)

		prob = 1

		for id in pts_dict.keys():
			prob *= 1 - sum([item[2] for item in pts_dict[id]])

		return prob

if __name__ == '__main__':
	# grid = GridMLSApprox('points-new.dat','id-new.dat')
	# xmax,ymax = grid.parse_file() #xmax = 58, ymax = 1.5
	# #print(grid.skyline_prob((46,0.979557)))
	# print(grid.skyline_prob((45.5,0.97)))
	#print(xmax,ymax)
	# grid.range_search_check((30,1))
	# heatmap_prob = np.zeros((75,58)) #58/1 = 58, 1.5/0.02 = 57 
	# for col in range(0,58): #points
	# 	for row in range(0,75): #true shooting percentage
	# 		heatmap_prob[row][col] = grid.skyline_prob((col+0.5,0.02*row+0.01))
	# np.savetxt('prob-matrix.txt',heatmap_prob)
	heatmap_prob = np.loadtxt('prob-matrix.txt')
	# heatmap_weights = heatmap_prob.flatten()
	# x = [item for item in np.arange(0.5,58,1)] * 75
	# y = [item*40 for item in np.arange(0.01,1.5,0.02)] * 58
	# heatmap, xedges, yedges = np.histogram2d(x, y, 
	# 	bins=(75,58),weights=heatmap_weights)
	# print(heatmap)
	cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
                                           ['blue','red'],
                                           256)

	img = plt.imshow(heatmap_prob,interpolation='nearest',
                    cmap = cmap,
                    origin='lower')
	plt.colorbar(img,cmap=cmap)
	# plt.minorticks_on()
	# # Customize the major grid
	# plt.grid(which='major', linestyle='-', linewidth='0.5', color='grey')
	# # Customize the minor grid
	# plt.grid(which='minor', linestyle='-', linewidth='0.5', color='grey')	
	plt.savefig('grid-skyline-approx.eps')
	#plt.show()
	

	# plt.imshow(heatmap_prob, cmap='hot', interpolation='nearest',origin='low')
	# plt.minorticks_on()
	# # Customize the major grid
	# plt.grid(which='major', linestyle='-', linewidth='0.5', color='grey')
	# # Customize the minor grid
	# plt.grid(which='minor', linestyle='-', linewidth='0.5', color='grey')	
	