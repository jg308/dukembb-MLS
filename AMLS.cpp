#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <queue> 
#include <ctime>
#include <cfloat>
#include <list>
#include <cstdint>
#include <climits>

using namespace std;
int n=19155;
//int n=18716;
int f=10;
int V;


int main(int argc, char** argv){
    ifstream infile("pointsNew.txt");
    string line;
    string delimiter1 = "|";
    string delimiter2 = ",";    
    vector<vector<double>> A(n);        
    int i=0;
    int j=0;
    
    i=0;
    j=0;
    int ll=0;
    int Ml;
    int qq;
    int Mq=0;
    while (getline(infile, line)){
        if(line=="|"){
            cout<<ll;
            exit(0);
        }    
        ll++;        
        size_t pos = 0;
        string token;
        j=0;
        qq=0;
        while ((pos = line.find(delimiter1)) != string::npos) {
            qq++;            
            token = line.substr(0, pos);
            size_t pos2 = 0;
            string token2;            
            int ggg=0;
            while ((pos2 = token.find(delimiter2)) != string::npos) {
                ggg++;
                token2 = token.substr(0, pos2);
                A[i].push_back((double)std::stof(token2, NULL));
                j++;
                token.erase(0, pos2 + delimiter2.length());
            }                    
            if(ggg==0){
                cout<<"Line: "<<ll;
                exit(0);
            }    
            line.erase(0, pos + delimiter1.length());
        }
        if(qq==0){
            cout<<"Line: "<<ll;
            exit(0);
        }    
        if(qq>Mq){
            Mq=qq;
            Ml=ll;
        }
        i++;
    }            

    srand (time(NULL));
    float prob;
    float sum;
    //long N=n*n*f*1000;
    long N=1000;
    
    list<list<pair<int,int>>> mylist;
    for(int k=1;k<=N;k++){                
        int temp[n];
       for(i=0;i<n;i++){
           prob=(float)rand()/INT_MAX;           
           sum=(float)0.0;
           for(int j=0;j<A[i].size();j=j+3){
               sum+=A[i][j+2];
               if(prob<=sum){
                   temp[i]=j;
                   break;
               }
           }
       }       
       list<pair<int,int>> l;
       for(int t=0;t<n;t++){            
           int t_counter=0;
           for(int r=0;r<n;r++){                   
               if(t==r)
                   continue;
               if(A[t][temp[t]]<= A[r][temp[r]] && A[t][temp[t]+1]<= A[r][temp[r]+1]){
                   t_counter++;
                   if(t_counter>1){
                    break;
                   }
               }
           }           
           if(t_counter<=1){
               pair<int, int> p=make_pair(t, temp[t]);
               l.push_back(p);
           }
       }
       mylist.push_back(l);
    }
        
    for (list<list<pair<int,int>>>::iterator it=mylist.begin(); it!=mylist.end() ; ++it){
        for (list<pair<int,int>>::iterator iter1=it->begin(); iter1!=it->end() ; ++iter1){
            cout<<"("<<iter1->first<<", "<<iter1->second<<") ";
        }
        cout<<"\n";
    }
        
    int maxF=-1;
    int maxEl;
    int i_tm=-1;
    int j_tm=-1;
    for (list<list<pair<int,int>>>::iterator it=mylist.begin(); it!=mylist.end() ; ++it){
        int localM=0;
        i_tm++;
        j_tm=-1;        
        for (list<list<pair<int,int>>>::iterator it2=mylist.begin(); it2!=mylist.end() ; ++it2){
            j_tm++;
            if(i_tm==j_tm){
                continue;
            }
            if(it->size()!=it2->size()){
                continue;
            }
            bool b=true;            
            for (list<pair<int,int>>::iterator iter1=it->begin(); iter1!=it->end() ; ++iter1){
                bool b2=false;
                for (list<pair<int,int>>::iterator iter2=it2->begin(); iter2!=it2->end() ; ++iter2){
                    if(iter1->first==iter2->first && iter1->second==iter2->second){
                        b2=true;
                        break;
                    }
                }
                if(b2==false){
                    b=false;
                    break;
                }
            }
            if(b==true){
                localM++;
            }            
        }
        if(localM>maxF){
            maxF=localM;
            maxEl=i_tm;
        }
    }
    maxF++;
    cout<<maxF<<" "<<maxEl<<"\n";
}
