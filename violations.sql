create or replace view rebVio as (
with rebViolation(pid,cnt) as
(
	select pid, count(*) from pgstats
	where dreb + oreb != treb
	group by pid
),
pidTotal(pid,cnt) as
(
	select pid,count(*) from pgstats
	group by pid
)

select r.pid,r.cnt as violations,t.cnt as total
from rebViolation r, pidTotal t
where r.pid = t.pid
);

create or replace view fgpVio as (
with fgpViolation(pid,cnt) as
(
	select pid, count(*) from pgstats
	where fg > fga
	group by pid
),
pidTotal(pid,cnt) as
(
	select pid,count(*) from pgstats
	group by pid
)

select r.pid,r.cnt as violations,t.cnt as total
from fgpViolation r, pidTotal t
where r.pid = t.pid
);

create or replace view ftpVio as (
with ftpViolation(pid,cnt) as
(
	select pid, count(*) from pgstats
	where ft > fta
	group by pid
),
pidTotal(pid,cnt) as
(
	select pid,count(*) from pgstats
	group by pid
)

select r.pid,r.cnt as violations,t.cnt as total
from ftpViolation r, pidTotal t
where r.pid = t.pid
);

create or replace view rebCleanPid as (
	select distinct pid from pgstats
	where pid not in (
		select pid from rebVio
		)
);

create or replace view fgpCleanPid as (
	select distinct pid from pgstats
	where pid not in (
		select pid from fgpVio
		)
);

create or replace view ftpCleanPid as (
	select distinct pid from pgstats
	where pid not in (
		select pid from ftpVio
		)
);