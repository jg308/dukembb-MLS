import psycopg2
import collections
from collections import defaultdict

conn = psycopg2.connect("dbname=dukembb user=vagrant")
if conn:
	print("Connected!")
else:
	print("Connection Error")

cur = conn.cursor()


def getDirtyFga(cur):
	# query = "select pid,fg,fga from pgstats where fga < fg;"
	query = "select distinct pid from fgpvio;"
	cur.execute(query)
	return cur.fetchall()

def getDirtyFta(cur):
	# query = "select pid,ft,fta from pgstats where fta < ft;"
	query = "select distinct pid from ftpvio;"
	cur.execute(query)
	return cur.fetchall()


def playerCleanFtaDist(cur):
	_playerDict = {}
	query = "select distinct pid from pgstats;"
	cur.execute(query)
	players = cur.fetchall() #get all players

	for p in players:
		pid = p[0]
		#get all clean ft-fta tuples for a given player
		cur.execute("select ft,fta from pgstats where pid = %s and fta >= ft;",(p)) 
		records = cur.fetchall()
		_playerDict[pid] = defaultdict(list)
		for r in records:
			_playerDict[pid][r[0]].append(r[1])

	return _playerDict

def playerCleanFgaDist(cur):
	_playerDict = {}
	query = "select distinct pid from pgstats;"
	cur.execute(query)
	players = cur.fetchall() #get all players

	for p in players:
		pid = p[0]
		#get all clean fg-fga tuples for a given player
		cur.execute("select fg,fga from pgstats where pid = %s and fga >= fg;",(p)) 
		records = cur.fetchall()
		_playerDict[pid] = defaultdict(list)
		for r in records:
			_playerDict[pid][r[0]].append(r[1])

	return _playerDict

def playerFgaAvg(cur):
	_fgaAvg = defaultdict(list)
	query = "select fg,fga from pgstats where fga >= fg;"
	cur.execute(query)
	records = cur.fetchall()
	for r in records:
		fg = r[0]
		fga = r[1]
		_fgaAvg[fg].append(fga)
	return _fgaAvg

def playerFtaAvg(cur):
	_ftaAvg = defaultdict(list)
	query = "select ft,fta from pgstats where fta >= ft;"
	cur.execute(query)
	records = cur.fetchall()
	for r in records:
		ft = r[0]
		fta = r[1]
		_ftaAvg[ft].append(fta)
	return _ftaAvg

def playerPosition(cur):
	cur.execute("select distinct pid from fgpcleanpid;")
	res = cur.fetchall()
	fgPid = [item[0] for item in res]
	cur.execute("select distinct pid from ftpcleanpid;")
	res = cur.fetchall()
	ftPid = [item[0] for item in res]

	_playerPos = {}
	_playerPosFgSummary = defaultdict(lambda:0)
	_playerPosFtSummary = defaultdict(lambda:0)
	with open("pid-position.csv",'r') as f:
		for line in f:
			pid,pos = line.split("|")
			if pid == "":
				continue
			pid = int(pid)
			_playerPos[pid] = pos.strip()
			if pid in fgPid:
				
				_playerPosFgSummary[pos.strip()] += 1
			if pid in ftPid:
				
				_playerPosFtSummary[pos.strip()] += 1
		f.close()
	#print(playerPos.keys())
	return _playerPos,_playerPosFgSummary,_playerPosFtSummary




fgaDict = playerCleanFgaDist(cur)
fgaDictAvg = playerFgaAvg(cur)
ftaDict = playerCleanFtaDist(cur)
ftaDictAvg = playerFtaAvg(cur)
playerPos,playerPosFgSummary,playerPosFtSummary = playerPosition(cur)

def playerCluster():
	_fgpCluster = defaultdict(list)
	_ftpCluster = defaultdict(list)
	_fgpClusterPos = defaultdict(list)
	_ftpClusterPos = defaultdict(list)
	_fgpClusterPosProb = {}
	_ftpClusterPosProb = {}
	#_playerPos,_playerPosFgSummary,_playerPosFtSummary = playerPosition()
	# print(set(_playerPos.values()))
	# print(_playerPosSummary)
	# sum1 = 0
	# sum2 = 0
	with open("cluster-5-kmeans-fgp.csv",'r') as f:
		for line in f:
			#sum1 += 1
			pid,tag = line.split("|")
			_fgpCluster[int(tag)].append(int(pid))
			if int(pid) in playerPos.keys():
				_fgpClusterPos[int(tag)].append(playerPos[int(pid)])
		f.close()
	with open("cluster-5-kmeans-ftp.csv","r") as f:
		for line in f:
			#sum2 += 1
			pid,tag = line.split("|")
			_ftpCluster[int(tag)].append(int(pid))
			if int(pid) in playerPos.keys():
				_ftpClusterPos[int(tag)].append(playerPos[int(pid)])
		f.close()
	for pos in ["G","G/F","F","F/C","C"]:
		sum = 0
		_fgpClusterPosProb[pos] = {}
		_ftpClusterPosProb[pos] = {}
		for key in [0,1,2,3,4]:
			counterFg = collections.Counter(_fgpClusterPos[key])
			counterFt = collections.Counter(_ftpClusterPos[key])
			if pos not in counterFg.keys():
				counterFg[pos] = 0
			if pos not in counterFt.keys():
				counterFt[pos] = 0
			sum += counterFg[pos]
			#P(cluster|position) = P(cluster,position)/P(position)
			_fgpClusterPosProb[pos][key] = counterFg[pos]/playerPosFgSummary[pos] 
			_ftpClusterPosProb[pos][key] = counterFt[pos]/playerPosFtSummary[pos]
		#print("{}-{}".format(sum,playerPosFgSummary[pos]))
	#print(_fgpClusterPosProb['G'])
		

	return _fgpCluster,_ftpCluster,_fgpClusterPosProb,_ftpClusterPosProb

fgpCluster,ftpCluster,fgpClusterPosProb,ftpClusterPosProb = playerCluster()



def cleanByAvg(pid,c,type):
	#print("clean by avg")
	if type == "fga":
		if c in fgaDictAvg.keys():
			counter = collections.Counter(fgaDictAvg[c])
			# for key in sorted(counter.keys()):
			# 	print("{}:{}".format(key,counter[key]/len(fgaDictAvg[c])))
			return {key:counter[key]/len(fgaDictAvg[c]) for key in sorted(counter.keys())}
		else:
			print("Oh no...")
	if type == "fta":
		if c in ftaDictAvg.keys():
			counter = collections.Counter(ftaDictAvg[c])
			# for key in sorted(counter.keys()):
			# 	print("{}:{}".format(key,counter[key]/len(ftaDictAvg[c])))
			return {key:counter[key]/len(ftaDictAvg[c]) for key in sorted(counter.keys())}
		else:
			print("Oh no...")

def cleanByOwn(pid,c,type):
	#print("Clean by own:" + type)
	#print(type)
	if type == "fga":
		if c in fgaDict[pid].keys():
			#print(fgaDict[pid][c])
			counter = collections.Counter(fgaDict[pid][c])
			# for key in sorted(counter.keys()):
			# 	print("{}:{}".format(key,counter[key]/len(fgaDict[pid][c])))
			return {key:counter[key]/len(fgaDict[pid][c]) for key in sorted(counter.keys())}
		else:
			return cleanByOthers(pid,c,type)
	if type == "fta":
		if c in ftaDict[pid].keys():
			#print(fgaDict[pid][c])
			counter = collections.Counter(ftaDict[pid][c])
			# for key in sorted(counter.keys()):
			# 	print("{}:{}".format(key,counter[key]/len(ftaDict[pid][c])))
			return {key:counter[key]/len(ftaDict[pid][c]) for key in sorted(counter.keys())}
		else:
			return cleanByOthers(pid,c,type)

def cleanByOthers(pid,c,type):
	#print("clean by others")
	if pid not in playerPos.keys():
		return cleanByAvg(pid,c,type)
	else:
		result = defaultdict(lambda:0)
		p_pos = playerPos[pid]
		if type == "fga":
			all_total = []
			for key in [0,1,2,3,4]:
				#print(fgpClusterPosProb[p_pos][key])
				total = []
				for player in fgpCluster[key]:
					total.extend(fgaDict[player][c])
					all_total.extend(total)
				counter = collections.Counter(total)
				if not counter:
					result[c] += 1 * fgpClusterPosProb[p_pos][key]
				else:
					for r in sorted(counter.keys()):
						result[r] +=  counter[r]/len(total) * fgpClusterPosProb[p_pos][key]
			if not all_total:
				return cleanByAvg(pid,c,type)
			else:
				return {key:result[key] for key in sorted(result.keys())}
			# sum = 0
			# for key in sorted(result.keys()):
			# 	sum += result[key]
			# 	#print("{}:{}".format(key,result[key]))
			# print(sum)
		#print("TODO")
		#do something
		if type == "fta":
			all_total = []
			for key in [0,1,2,3,4]:
				total = []
				for player in ftpCluster[key]:
					total.extend(ftaDict[player][c])
					all_total.extend(total)
				counter = collections.Counter(total)
				if not counter:
					result[c] += 1 * ftpClusterPosProb[p_pos][key]
				else:
					for r in sorted(counter.keys()):
						result[r] += counter[r]/len(total) * ftpClusterPosProb[p_pos][key]
			# for key in sorted(result.keys()):
			# 	print("{}:{}".format(key,result[key]))
			if not all_total:
				return cleanByAvg(pid,c,type)
			else:
				return {key:result[key] for key in sorted(result.keys())}
			# sum = 0
			# for key in sorted(result.keys()):
			# 	sum += result[key]
			# 	#print("{}:{}".format(key,result[key]))
			# print(sum)

def trueShootPer(tp,fga,fta):
	return tp/float(2*fga + float(0.88*fta))

def expectation(dict):
	result = 0
	for key in dict.keys():
		result += key * dict[key]
	return int(result)

def impute(cur,pid,c,type):
	#query = "select pid from pgpvio;"
	if type == "fga":
		cur.execute("select violations,total from fgpvio where pid = %s;",(pid,))
	if type == "fta":
		cur.execute("select violations,total from ftpvio where pid = %s;",(pid,))
	records = cur.fetchall()
	size = len(records)
	assert(size == 1 or size == 0)
	if size == 1:
		vio = int(records[0][0])
		total = int(records[0][1])
		if vio < total/2 : 
			return cleanByOwn(pid,c,type)
		else:
			return cleanByOthers(pid,c,type)
	else:
		print("clean")

def imputeAll(records,ids,log):
	output = open(records,'w')
	idfile = open(ids,'w')
	logfile = open(log,'w')
	cur.execute("select pid,fg,fga,ft,fta,gid,tp from pgstats where fga < fg or fta < ft;")
	#cur.execute("select pid,fg,fga,ft,fta,gid,tp from pgstats where pid=159 and gid=19520229")
	dirtyRecords = cur.fetchall()
	#print(dirtyRecords)
	
	for r in dirtyRecords:
		fga={}
		fta={}
		print("===records:{}===".format(r),file=logfile)
		#fg > fga?
		if r[1] > r[2]: 
			fga = impute(cur,r[0],r[1],"fga")
		else:
			fga[r[2]] = 1.0
		print("fga-distribution:{}".format(fga),file=logfile)
		#ft > fta?
		if r[3] > r[4]:
			fta = impute(cur,r[0],r[3],"fta")
		else:
			fta[r[4]] = 1.0
		print("fta-distribution:{}".format(fta),file=logfile)
		#at least one of the two dictionaries should be non-empty: since we impute the dirty entry
		assert(bool(fga) or bool(fta))

		# result = [(r[6],trueShootPer(r[6],k1,k2),fga[k1]*fta[k2]) for k1 in fga.keys() for k2 in fta.keys()]
		result = [(r[6],trueShootPer(r[6],k1,k2),fga[k1]*fta[k2]) for k1 in fga.keys() for k2 in fta.keys()]
		#result = (r[6],expectation(fga),expectation(fta))
		
		print("{},{},".format(r[0],r[5]),file=idfile) #pid,gid

		#aggregated-by-player view
		# print("{},{},{},".format(result[0],result[1],result[2]),file=output)
		# print("{},{},{},".format(result[0],result[1],result[2]),file=logfile)

		#per game view
		print("|".join("%d,%f,%f," % x for x in result)+'|',file=output)
		print("|".join("%d,%f,%f," % x for x in result),file=logfile)

	cur.execute("select pid,gid,tp,fga,fta from pgstats where fga >= fg and fta >= ft and tp != 0 and not (fta=0 and fga=0);")
	cleanRecords = cur.fetchall()
	for r in cleanRecords:
		print("{},{},".format(r[0],r[1]),file=idfile)
		print("{},{},{},|".format(r[2],trueShootPer(r[2],r[3],r[4]),1),file=output)
		#print("{},{},{},{},|".format(r[2],r[3],r[4],1),file=output)
		print("{},{},{},{},{}".format(r[0],r[1],r[2],r[3],r[4]),file=logfile)
		
	output.close()
	logfile.close()
	idfile.close()

def getCleanTuples(file):
	output = open(file,'w')
	cur.execute("select pid,fg,fga,ft,fta,gid from pgstats where fga >= fg and fta >= ft and tp != 0;")
	cleanRecords = cur.fetchall()
	for r in cleanRecords:
		print("{},{}".format(r[0],r[5]),file=output)
		print("{}|{}|{}".format(r[2],r[4],1),file=output)
	output.close()



if __name__ == '__main__':

	imputeAll("points-new.dat","id-new.dat","output-new.log")
	#print(impute(cur,301,16,"fta"))
