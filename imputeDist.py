import psycopg2

conn = psycopg2.connect("dbname=dukembb user=vagrant")
if conn:
	print("Connected!")
else:
	print("Connection Error")

cur = conn.cursor()

# cur.execute("select pid from fgpcleanpid;")
# validPid = cur.fetchall()

# DATA = open('fgp-distribution.csv','w')
# for player in validPid:
# 	print(player[0])
# 	cur.execute("select fgp from pgstats where pid=%s",(player))
# 	values = cur.fetchall()
# 	fgpDist = {}
# 	for i in range(-1,101):
# 		fgpDist[i] = 0
# 	for v in values:
# 		if v[0] is None:
# 			fgpDist[-1] = fgpDist[-1] + 1
# 		else:
# 			key = int(v[0]*100)
# 			fgpDist[key] = fgpDist[key] + 1

# 	DATA.write(str(player[0])+':'+','.join(str(fgpDist[i]) for i in range(-1,101)))
# 	DATA.write('\n')
# DATA.close()

cur.execute("select pid from ftpcleanpid;")
validPid = cur.fetchall()

DATA = open('ftp-distribution.csv','w')
for player in validPid:
	print(player[0])
	cur.execute("select ftp from pgstats where pid=%s",(player))
	values = cur.fetchall()
	ftpDist = {}
	for i in range(-1,101):
		ftpDist[i] = 0
	for v in values:
		if v[0] is None:
			ftpDist[-1] = ftpDist[-1] + 1
		else:
			key = int(v[0]*100)
			ftpDist[key] = ftpDist[key] + 1

	DATA.write(str(player[0])+':'+','.join(str(ftpDist[i]) for i in range(-1,101)))
	DATA.write('\n')
DATA.close()